import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class ExamTestTest {

    ExamTestInterface examTested;

    @Before
    public void init(){
        examTested = new ExamTest();
    }

    @Test
    public void fetch(){
        assertEquals(1, examTested.fetch(0));
        assertEquals(1, examTested.fetch(1));
        assertEquals(0, examTested.fetch(3));
        assertEquals(0, examTested.fetch(-1));
        assertEquals(0, examTested.fetch(Integer.MAX_VALUE));
        assertEquals(0, examTested.fetch(Integer.MIN_VALUE));
    }
}
